# My uClinux m68k libreswan playground #

A work-in-progress ...


## Branches ##

There are two branches.


### master ###

Stuff that has been cleaned up, and, on occasion, pushed upstream.  It
is missing changes needed to build libreswan.

To help keep track of what is where, the patches have a somewhat
querky naming convention:

- *-todo-* needs further work

- *-pend-* for patches wait submition

- *-hold-* for patches submitted upstream but on-hold awaiting approval

- *-done-* for patches accepted upstream

See the individual patches for details.


### libreswan-wip ###

A huge mess.  On the other hand, it does build a working libreswan.
Keep in mind that this branch gets forced updates.


## Building the work-in-progress branch for Coldfire ##

For the moment:

- install a debian squeeze based distro on a VM (the m68k tool chain
  is really old)

- download an install the m68k toolchain from
  http://www.uclinux.org/pub/uClinux/m68k-elf-tools/

- check out the libreswan-wip branch

- use "make menuconfig" to select Freescale:M5802LSW (it enables IP and crypto stuff in the kernel, along with everything that should build).

- build with "make"

It creates a romfs that includes pluto, whack, and addconn binaries.
There's also certutil and pk12util but they are just links to pluto.


## Issues ##

What problems have so far been identified?


### lib/libgmp ###

Updated to latest.

- custom INSTALL rule that just installs libgmp.a and gmp.h

  by default info files and shared libraries are installed

- while there's a menuconfig option to enable/disable assembly
  I don't seem to get it to save the value regardless of libgmp
  being enabled/disabled.  This means that even though libreswan
  can enable libgmp there isn't a way to automatically disable
  assembly on boards that need it


### lib/libevent ###

This is missing.

It will need to be added before libreswan can be updated to current.


### lib/nspr ###

Suggestions:

- shared library builds are disabled by clobering SHARED_LIBRARY
  perhaps it should be configurable

- see if the configure options that were purged are needed

- replace the fork() call with vfork(), for moment it fudges a fork-fail


### lib/nss ###


### user/libreswan ###
