#!/bin/sh
#
# take care of 8300 specific LED setting
#
# The most important thing here is to ensure that a LED is always on.
# if we have no LAN and no cell,  that means keep the ETH led flashing.
#
##############################################################
# allow script override
[ -x /etc/config/led.sh ] && exec /etc/config/led.sh "$@"
##############################################################

usage()
{
	[ "$1" ] && echo "$1"
	echo "usage: $0 <interface_lan|interface_modem> <setup|teardown|up|down>"
	exit 1
}

##############################################################

lan_led()
{
	case "$1" in
	setup)         ledcmd -f ONLINE ;;
	up)            ledcmd -o ONLINE ;;
	down|teardown) ledcmd -O ONLINE ;;
	esac
}

modem_led()
{
	case "$1" in
	setup)         ledcmd -f USB ;;
	up)            ledcmd -o USB ;;
	down|teardown) ledcmd -O USB ;;
	esac
}

signal_led()
{
	sig=""
	[ "$1" -ge 81 ] && sig="$sig -o RSS5"
	[ "$1" -lt 81 ] && sig="$sig -O RSS5"
	[ "$1" -ge 62 ] && sig="$sig -o RSS4"
	[ "$1" -lt 62 ] && sig="$sig -O RSS4"
	[ "$1" -ge 43 ] && sig="$sig -o RSS3"
	[ "$1" -lt 43 ] && sig="$sig -O RSS3"
	[ "$1" -ge 24 ] && sig="$sig -o RSS2"
	[ "$1" -lt 24 ] && sig="$sig -O RSS2"
	[ "$1" -ge 5 ]  && sig="$sig -o RSS1"
	[ "$1" -lt 5 ]  && sig="$sig -O RSS1"
	[ -x /usr/bin/ftdi-led ] && /usr/bin/ftdi-led $sig 2> /dev/null
}

rat_led()
{
	rat=""
	case "$1" in
	3g) rat="-o 3g -O 4g" ;;
	4g) rat="-O 3g -o 4g" ;;
	*)  ;;
	esac
	[ -x /usr/bin/ftdi-led ] && /usr/bin/ftdi-led $rat 2> /dev/null
}

##############################################################

[ $# -ne 2 ] && usage "Wrong number of arguments"

CMD="$1"
shift

case "$CMD" in
interface_lan*)   lan_led    "$@" ;;
interface_modem*) modem_led  "$@" ;;
signal*)          signal_led "$@" ;;
rat*)             rat_led    "$@" ;;
*)                exit 1 ;;
esac

exit 0
