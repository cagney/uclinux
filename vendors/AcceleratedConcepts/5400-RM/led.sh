#!/bin/sh
#
# take care of 5400-RM specific LED setting
#
# The most important thing here is to ensure that a LED is always on.
# if we have no LAN and no cell,  that means keep the ETH led flashing.
#
##############################################################
# allow script override
[ -x /etc/config/led.sh ] && exec /etc/config/led.sh "$@"
##############################################################

usage()
{
	[ "$1" ] && echo "$1"
	echo "usage: $0 <interface_lan|interface_modem|signal|rat> <setup|teardown|up|down>"
	exit 1
}

##############################################################
#
# get our tools
#

source /usr/share/libubox/jshn.sh

##############################################################

lan_led()
{
	LAN4=-1
	STATUS=$(ubus call network.interface.ipv4_interface_lan status 2> /dev/null)
	if [ "$?" = 0 ]; then
		LAN4=0
		eval $(jshn -r "$STATUS" 2> /dev/null)
		json_get_var pending "pending"
		json_get_var up "up"
		[ "${pending:-0}" -eq 1 ] && LAN4=1
		[ "${up:-0}" -eq 1 ] && LAN4=2
	fi

	LAN6=-1
	STATUS=$(ubus call network.interface.ipv6_interface_lan status 2> /dev/null)
	if [ "$?" = 0 ]; then
		LAN6=0
		eval $(jshn -r "$STATUS" 2> /dev/null)
		json_get_var pending "pending"
		json_get_var up "up"
		[ "${pending:-0}" -eq 1 ] && LAN6=1
		[ "${up:-0}" -eq 1 ] && LAN6=2
	fi

	# if neither interface exists force the LAN to be up (ie., passthrough)
	[ "$LAN4" -eq -1 -a "$LAN6" -eq -1 ] && LAN4=2

	if [ "$LAN4" -eq 2 -o "$LAN6" -eq 2 ]; then
		ledcmd -o ETH
	elif [ "$LAN4" -eq 1 -o "$LAN6" -eq 1 ]; then
		ledcmd -f ETH
	else
		ledcmd -O ETH
	fi
}

modem_led()
{
	case "$1" in
	setup)         ledcmd -f ONLINE ;;
	up)            ledcmd -o ONLINE ;;
	down|teardown) ledcmd -O ONLINE ;;
	esac
}

signal_led()
{
	sig=""
	[ "$1" -ge 81 ] && sig="$sig -o RSS5"
	[ "$1" -lt 81 ] && sig="$sig -O RSS5"
	[ "$1" -ge 62 ] && sig="$sig -o RSS4"
	[ "$1" -lt 62 ] && sig="$sig -O RSS4"
	[ "$1" -ge 43 ] && sig="$sig -o RSS3"
	[ "$1" -lt 43 ] && sig="$sig -O RSS3"
	[ "$1" -ge 24 ] && sig="$sig -o RSS2"
	[ "$1" -lt 24 ] && sig="$sig -O RSS2"
	[ "$1" -ge 5 ]  && sig="$sig -o RSS1"
	[ "$1" -lt 5 ]  && sig="$sig -O RSS1"
	ledcmd $sig
}

rat_led()
{
	case "$1" in
	3g) ledcmd -O LAN3_RX -o LAN3_TX ;;
	4g) ledcmd -o LAN3_RX -O LAN3_TX ;;
	*)  ledcmd -O LAN3_RX -O LAN3_TX ;;
	esac
}

firmware_led()
{
	case "$1" in
	start) ledcmd -a -n ALL -f ALL ;;
	stop)  ledcmd -a -N ALL ;;
	esac
}

##############################################################

[ $# -ne 2 ] && usage "Wrong number of arguments"

CMD="$1"
shift

case "$CMD" in
*interface_lan*)  lan_led    "$@" ;;
interface_modem*) modem_led  "$@" ;;
signal*)          signal_led "$@" ;;
rat*)             rat_led    "$@" ;;
firmware*)        firmware_led "$@" ;;
*)                exit 1 ;;
esac

exit 0
