#!/bin/bash

# we need our tools
export PATH="/usr/local/bin:$PATH"
# do not install anything into /tftpboot
export NO_BUILD_INTO_TFTPBOOT=1
# one date for all builds
export BUILD_START_STRING=$(date -u -R)
# we are the build machine,  no hashes in version
export ON_BUILD_MACHINE=1

TARGETS=
TARGETS="$TARGETS AcceleratedConcepts/5300-DC"
TARGETS="$TARGETS AcceleratedConcepts/5301-DC"
TARGETS="$TARGETS AcceleratedConcepts/5400-RM"
TARGETS="$TARGETS AcceleratedConcepts/6300-CX"
TARGETS="$TARGETS AcceleratedConcepts/6300-LX"

# clean out releases for each build
rm -rf release

PASS=0
FAIL=0
PASS_LIST=
for target in $TARGETS; do
    echo "BUILDING: ${target}"
    if make ${target}_default && make release; then
        echo "INFO: ${target} complete"
        PASS=$((PASS + 1))
        PASS_LIST="$PASS_LIST:$target:"
    else
        echo "ERROR: ${target} failed"
        FAIL=$((FAIL + 1))
    fi
done

echo "BUILDING: results"
for target in $TARGETS; do
    case "$PASS_LIST" in
    *:$target:*) echo "PASS: $target" ;;
    *)           echo "FAIL: $target" ;;
    esac
done

echo "---------------------------------------------"
echo "$PASS builds passed, $FAIL builds FAILED."
echo "---------------------------------------------"

exit $FAIL
