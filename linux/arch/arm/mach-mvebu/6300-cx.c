/*
 * Support for AcceleratedConcepts 6300-CX
 * David McCullough <david.mccullough@accelecon.com>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/mv643xx_eth.h>
#include <linux/ethtool.h>
#include <linux/delay.h>
#include <linux/of.h>
#include <linux/gpio.h>
#include <asm/io.h>
#include "common.h"
#include "kirkwood.h"
#include "board.h"

static __init int accelecon_6300_cx_fixup(void)
{
	struct device_node *np;

	/* if we are using all of NAND then nothing to do here */
	if (strstr(boot_command_line, "fullnand"))
		return 0;

	pr_info("6300-CX NAND partition fixup\n");

	/*
	 * find the nand flash and then find the 'flash' partition
	 * change its size if needed :-)
	 */
	np = of_find_node_by_name(NULL, "nand");
	if (np) {
		struct device_node *npp;

		for_each_child_of_node(np, npp) {
			struct property *lp, *rp;
			int rlen, llen, mtd_len;

			llen = 0;
			lp = of_find_property(npp, "label", &llen);
			if (!lp)
				continue;

			/* check the label for the MTD names we want */
			mtd_len= 0;
			if (strcmp(lp->value, "flash") == 0)
				mtd_len = 0x07b00000;
			if (strcmp(lp->value, "all") == 0)
				mtd_len = 0x08000000;
			if (mtd_len == 0)
				continue;
			/*
			 * change the second value (segment size) in the
			 * reg property for the partition
			 */
			rlen = 0;
			rp = of_find_property(npp, "reg", &rlen);
			if (rp && (rp->length == 8)) {
				pr_info("6300-CX NAND partition fixed: %s\n",
					(char *) lp->value);
				((u32 *)rp->value)[1] = cpu_to_be32(mtd_len);
			}
		}
		of_node_put(np);
	}
	return 0;
}
early_initcall(accelecon_6300_cx_fixup);


